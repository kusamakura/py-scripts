#!/usr/bin/env python3
# Usage:
#   - dota2replaydl.py <downloads file> <destination directory>
#   - Downloads file contains download urls separated by line breaks

import sys
import os
import os.path
import subprocess
import errno
import urllib.parse
import datetime


def log(message, file='dota2replaydl.log'):
    """
    Logs messages to a log file.
    """
    try:
        logfile = open(file, 'a')
        logfile.write('[%s] %s\n' %
                      (datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                       message))
        logfile.close()
    except Exception as e:
        print('Error logging:', e)


def try_shcall(command, log_on_success, log_on_failure):
    """
    Wrapper for a shell command call in a try/except block.
    """
    try:
        retcode = subprocess.call(command, shell=True)
        if retcode == 0:
            print(log_on_success)
            log(log_on_success)
        else:
            print(log_on_failure)
            log(log_on_failure)
    except OSError as e:
        print('Error:', e)
        log('Error:', e)


# Unpack arguments
this_script, queue_file, dest_dir = sys.argv

# Nothing to download, so exit
if not os.path.exists(queue_file):
    print('Downloads file <%s> not found.' % queue_file)
    exit(1)

# Fall back on current dir if dest_dir doesn't exist
try:
    os.makedirs(dest_dir)
except OSError as e:
    if e.errno != errno.EEXIST:
        dest_dir = '.'

# Read the queue file
f = open(queue_file, 'r')
queue = f.read()
f.close()

# Download the items in queue!
# TODO: Periodically check the text file for queue updates
for download in queue.split('\n'):
    if not download == '':
        # Get file name from URL
        parse_url = urllib.parse.urlparse(download)
        save_as = os.path.join(dest_dir + '/',
                               os.path.split(parse_url.path)[-1])
        print('Downloading:', download)
        # TODO: Should probably use urllib for this
        try_shcall('curl ' + download + ' -o ' + save_as,
                   'Downloaded: ' + download, 'Failed: ' + download)

# Unzip all using 7zip
# TODO: Should probably use the bz2 library
try_shcall('7z e ' + dest_dir + '/' + '*.dem.bz2 -y',
           'Success: 7zip', 'Failure: 7zip')
