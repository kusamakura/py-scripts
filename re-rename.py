#!/usr/bin/env python3
import os
import re
import sys
import glob


try:
    scriptname, file_filter, find, replace = sys.argv
except:
    print("Invalid arguments. Usage:\n "
          "scriptname.py file-filter find-pattern replace-pattern")
    sys.exit()

for file in glob.iglob(file_filter):
    try:
        newfile = re.sub(find, replace, file)
        print("Renaming %s to %s." % (file, newfile))
        os.rename(file, newfile)
    except:
        print("Error renaming %s." % file)
